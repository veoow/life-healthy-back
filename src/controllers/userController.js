const bcrypt = require('bcrypt')
const UserModel = require('../models/user-models')
const jwt = require('jsonwebtoken')

const authConfig = require('../config/auth.json')

module.exports = {
  async store (req, res) {
    try {
      const { email, username } = req.body
      if (await UserModel.findOne({ email }) || await UserModel.findOne({ username })) return res.status(400).json({ error: 'Usario já existente' })
      const user = await UserModel.create(req.body)
      return res.json(user)
    } catch (err) {
      console.log(err)
      return res.send(err)
    }
  },

  async auth (req, res) {
    const { username, password, email } = req.body
    try {
      const userParam = username ? { username } : { email }

      const user = await UserModel.findOne(userParam).select('+password')

      if (!user) return res.status(400).json('Usuario não encontrado')

      if (!await bcrypt.compare(password, user.password)) return res.status(400).json('Invalid password')
      user.password = undefined

      const token = jwt.sign({ id: user._id }, authConfig.secret, {
        expiresIn: '7d'
      })

      return res.json({ user, token })
    } catch (err) {
      return res.status('500').json(err)
    }
  }
}
