const RecordModel = require('../models/records-model')

module.exports = {
  async index (req, res) {
    try {
      const { user_id } = req.params

      console.log(user_id)
      const records = await RecordModel.find({ user_id: user_id })
      return res.status(200).json({ records })
    } catch (error) {
      return res.status(500).json({ err: error })
    }
  },

  async store (req, res) {
    try {
      const newRecord = await RecordModel.create(req.body)

      req.io.emit('New Record', { newRecord })
      return res.status(200).json({ newRecord })
    } catch (error) {
      return res.status(500).json({ err: error })
    }
  }
}
