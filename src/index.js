const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')

const app = express()

const server = require('http').Server(app)
const io = require('socket.io')(server)

require('dotenv').config()
mongoose.connect(process.env.MONGO_DB_URL, {
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true
})
  .then(() => console.log('Connected to MONGO DB'))
  .catch(err => console.log(err))

app.use(cors())
app.use((req, res, next) => {
  req.io = io
  return next()
})

app.use(express.json())
app.use(require('./routes'))

server.listen(process.env.PORT || 3001, () => {
  console.log('Server is running now!')
})
module.exports = app
