const express = require('express')
const expressGraphql = require('express-graphql')

// const resolvers = require('./resolvers')
const schema = require('./schemas/schema')

const app = express()

app.use('/user', expressGraphql({ schema: schema, pretty: true }))
